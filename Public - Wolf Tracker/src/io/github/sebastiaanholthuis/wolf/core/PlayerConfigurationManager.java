package io.github.sebastiaanholthuis.wolf.core;

import java.util.HashMap;

public class PlayerConfigurationManager {

	private WolfTracker plugin;
	
	private HashMap<String, PlayerConfiguration> configs = new HashMap<String, PlayerConfiguration>();

	public PlayerConfigurationManager(WolfTracker instance) {
		this.plugin = instance;
	}
	
	public HashMap<String, PlayerConfiguration> getConfigs() {
		return configs;
	}

	public void setConfigs(HashMap<String, PlayerConfiguration> configs) {
		this.configs = configs;
	}
	
}
