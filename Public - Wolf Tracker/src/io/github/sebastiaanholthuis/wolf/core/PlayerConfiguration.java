package io.github.sebastiaanholthuis.wolf.core;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayerConfiguration {

	private WolfTracker plugin;
	private Player player;
	
	private File baseFolder;
	private File playerFile;
	
	private FileConfiguration playerConfiguration;
	
	public PlayerConfiguration(WolfTracker instance, Player player) {
		this.plugin = instance;
		this.player = player;
		
		initializeFiles();
		initializeConfig();
		
		createPaths();
		
		plugin.getPlayerConfigurationManager().getConfigs().put(player.getName(), this);
	}
	
	private void initializeFiles() {
		baseFolder = new File(plugin.getDataFolder() + File.separator + "players");
		
		if (!baseFolder.exists()) {
			baseFolder.mkdirs();
		}
		
		playerFile = new File(baseFolder + File.separator + player.getName() + ".yml");
		
		if (!playerFile.exists()) {
			try {
				playerFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initializeConfig() {
		playerConfiguration = YamlConfiguration.loadConfiguration(playerFile);
	}
	
	private void createPaths() {
		if (!playerConfiguration.contains("kills.tamed")) {
			playerConfiguration.set("kills.tamed", 0);
		}
		
		if (!playerConfiguration.contains("kills.untamed")) {
			playerConfiguration.set("kills.untamed", 0);
		}
		
		savePlayerConfiguration();
	}
	
	public FileConfiguration getPlayerConfiguration() {
		return playerConfiguration;
	}
	
	public void savePlayerConfiguration() {
		try {
			playerConfiguration.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
