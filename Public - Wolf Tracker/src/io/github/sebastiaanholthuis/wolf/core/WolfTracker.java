package io.github.sebastiaanholthuis.wolf.core;

import io.github.sebastiaanholthuis.wolf.listeners.EntityListener;
import io.github.sebastiaanholthuis.wolf.listeners.PlayerListener;

import org.bukkit.plugin.java.JavaPlugin;

public class WolfTracker extends JavaPlugin {

	private PlayerConfigurationManager pcm;
	
	public void onEnable() {
		createInstances();
		
		registerListeners();
		registerCommands();
	}
		
	private void createInstances() {
		pcm = new PlayerConfigurationManager(this);
	}
	
	private void registerListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		getServer().getPluginManager().registerEvents(new EntityListener(this), this);
	}
	
	private void registerCommands() {}
	
	public PlayerConfigurationManager getPlayerConfigurationManager() {
		return pcm;
	}
	
}
