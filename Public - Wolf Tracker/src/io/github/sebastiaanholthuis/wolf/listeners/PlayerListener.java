package io.github.sebastiaanholthuis.wolf.listeners;

import io.github.sebastiaanholthuis.wolf.core.PlayerConfiguration;
import io.github.sebastiaanholthuis.wolf.core.WolfTracker;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

	private WolfTracker plugin;
	
	public PlayerListener(WolfTracker instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		PlayerConfiguration pc = new PlayerConfiguration(plugin, player);
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		if (plugin.getPlayerConfigurationManager().getConfigs().containsKey(player.getName())) {
			plugin.getPlayerConfigurationManager().getConfigs().remove(player.getName());
		}
	}
	
}
