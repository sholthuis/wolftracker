package io.github.sebastiaanholthuis.wolf.listeners;

import io.github.sebastiaanholthuis.wolf.core.PlayerConfiguration;
import io.github.sebastiaanholthuis.wolf.core.WolfTracker;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityListener implements Listener {

	private WolfTracker plugin;
	
	public EntityListener(WolfTracker instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		Entity entity = event.getEntity();
		
		if (entity instanceof Wolf) {
			Wolf wolf = (Wolf) entity;
			
			if (wolf.getKiller() instanceof Player) {
				Player player = (Player) wolf.getKiller();
				
				if (plugin.getPlayerConfigurationManager().getConfigs().containsKey(player.getName())) {
					PlayerConfiguration pc = plugin.getPlayerConfigurationManager().getConfigs().get(player.getName());
					
					if (wolf.isTamed()) {
						pc.getPlayerConfiguration().set("kills.tamed", pc.getPlayerConfiguration().getInt("kills.tamed") + 1);
					} else {
						pc.getPlayerConfiguration().set("kills.untamed", pc.getPlayerConfiguration().getInt("kills.untamed") + 1);
					}
					
					pc.savePlayerConfiguration();
				}
			}
		}
	}
	
}
